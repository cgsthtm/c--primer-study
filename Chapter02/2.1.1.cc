#include <iostream>

int main()
{
    /*
    C++ 算术类型：
    bool 布尔类型 未定义
    char 字符 8位
    wchar_t 宽字符 16位
    char16_t Unicode字符 16位
    char32_t Unicode字符 32位
    short 短整型 16位
    int 整型 16位
    long 长整型 32位
    long long 长整型 64位
    float 单精度浮点数 6位有效数字
    double 双精度浮点数 10位有效数字
    long double 扩展精度浮点数 10位有效数字
    */

    signed char ts = -128; // 大多数现代计算机上，带符号的chat的表示范围： -128至127
    std::cout << ts << std::endl;

    

    return 0;
}