/**
 * @brief 2.6.1 自定义数据结构 自定义Sales_data类型
 * 定义类可以用struct关键字
 * @return int 
 */

#include <iostream>

/**
 * 创建对象时，类内初始值用于初始化数据成员。
 * 没有初始值的成员将被默认初始化。
 * 因此当定义Sales_data的对象时，units_sold和revenue都将初始化为0，bookNo将初始化为空字符串。
 */
struct Sales_data
{
    std::string bookNo;
    unsigned units_sold = 0; // unsigned是unsigned int的缩写
    double revenue = 0.0;
}; // 类必须以分号;结尾


int main()
{
    return 0;
}