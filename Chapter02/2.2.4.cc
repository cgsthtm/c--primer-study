/**
 * @brief 2.2.4 名字的作用域
 * 同一个名字在不同的作用域中可能指向不同的实体。
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
#include <iostream>

// 函数内部不宜定于于全局变量同名的新变量
int reused = 42; // reused拥有全局作用域
int main(int argc, char const *argv[])
{
    int unique = 0; // 块级作用域
    // 输出#1：使用全局变量reused，输出42 0
    std::cout << reused << " " << unique << std::endl;

    int reused = 0; // 新建局部变量，与全局变量名一致，覆盖了全局变量名
    // 输出#2：使用局部变量reused，输出0 0
    std::cout << reused << " " << unique << std::endl;

    // 输出#3：使用全局变量reused，输出42 0
    std::cout << ::reused << " " << unique << std::endl;
    

    int sum = 0;
    return 0;
}
