/**
     * @brief 如果想声明一个变量而非定义它，就在变量名前添加关键字extern，而且不要显式地初始化变量
     * extern语句如果包含初始值就不再是声明，而变成定义了
     * 
     */

#include <iostream>

extern int i; // 声明i而非定义i
int j; // 声明并定义j
// extern double pi = 3.14; // 定义pi  编译时会报警告：warning: 'pi' initialized and declared 'extern'

// 变量能且只能被定义一次，但是可以被多次声明。
extern int a;
extern int a; // 编译通过

int main()
{
    // std::cout << i << std::endl; // 编译时报错：undefined reference to `i'
    std::cout << j << std::endl;
    // std::cout << pi << std::endl;

    extern int b;
    extern int b; // 函数体内部也可以用extern声明变量，但好像没啥用

    return 0;
}