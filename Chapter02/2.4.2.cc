/**
 * @file 2.4.2.cc
 * @author your name (you@domain.com)
 * @brief 2.4.2 指针和const
 * @version 0.1
 * @date 2021-12-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <iostream>

int main()
{
    // 指向常量的指针，与指向常量的引用类似，都不能改变其所指对象的值。
    const double pi = 3.14;
    // double *ptr = &pi; // 错误，ptr是一个普通指针，其实应该是个指向常量的指针才对
    const double *cptr = &pi; // 正确
    // *cptr = 42; // 错误，不能给*ptr指向的常量赋值
    std::cout << *cptr << std::endl;
    double bi = 2.22;
    const double *cptr1 = &bi; // 正确，与指向常量的引用类似，允许一个指向常量的指针指向一个非常量对象
    std::cout << *cptr1 << std::endl;
    cptr1 = &pi; // 正确，可以修改指向常量的指针，让他指向另一个常量对象
    std::cout << *cptr1 << std::endl;

    /**
     * @brief 指向常量的指针，和常量指针是两个概念。
     * 指向常量的指针，指的是，这个指针指向了一个常量，不能通过这个指针修改它所指对象的值。
     * 常量指针，指的是，这个指针本身是一个常量，不可以给这个常量指针再赋值（也就是说不允许修改它所存放的地址）。
     * 
     */
    int errNumb = 0;
    int *const currErr = &errNumb; // currErr将一直指向errNumb
    const double pi = 3.14;
    const double *const ppi = &pi; // ppi是一个指向常量对象的常量指针
    // *ppi = 2.22; // 错误，ppi所指对象是一个常量，所以不能修改这个常量的值
    if(*currErr)
    {
        
    }


    return 0;
}