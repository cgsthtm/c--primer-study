#include <iostream>

int main()
{
    /**
     * 类型转换
     * 
     */
    bool b = 42; // b为真
    int i = b; // i的值为1
    i = 3.14; // i的值为3
    double pi = i; // pi的值为3.0
    unsigned char c = -1; // 假设char占8比特，c的值为255（-1对256取模=255，之所以对256取模是因为unsigned char表示的范围是0~255，共256个值）
    signed char c2 = 256; // 假设char占8比特，c2的值是未定义的
    
    return 0;
}