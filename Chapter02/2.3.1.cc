/**
 * @brief 2.3.1 引用
 * 引用（reference）为对象起了另外一个名字，引用类型引用（refers to）另外一种类型。
 * 引用并非对象，相反的，它只是为一个已经存在的对象所起的另外一个名字。
 * @return int 
 */
#include <iostream>

int main()
{
    int iVal = 1024;
    int &refVal = iVal; // refVal指向iVal（是iVal的另一个名字）
    // int &refVal2; // 报错，引用必须被初始化
    std::cout << iVal << std::endl;

    refVal = 2; // 将2赋值给refVal指向的iVal
    std::cout << iVal << std::endl; // 输出2
    
    int ii = refVal; // ii=iVal
    std::cout << iVal << std::endl; // 输出2

    int &refVal3 = refVal; // 正确，refVal3指向了iVal

    // int &refVal4 = 10; // 错误，引用类型的初始值必须是一个对象
    int iVal2 = 888;
    // &refVal = iVal2; // 错误，不能改变引用使其指向不同的对象（而指针可以）。
    return 0;
} 