#include <iostream>
#include "../Chapter01/Sales_item.h"

int main()
{
    /**
     * @brief 变量定义
     * 
     */

    int sum = 0, value, uint_sold = 0; 
    Sales_item item;
    std::string book("0-201-78345-X"); // book通过一个string字面值初始化

    int uint_sold1 = 0;
    int uint_sold2 = {0}; // 列表初始化
    int uint_sold3 {0}; // 列表初始化
    int uint_sold4 (0);

    long double ld = 3.1415926536;

    // 如果我们使用列表初始化且初始值存在丢失信息的风险，则编译器将报错：
    // int a{ld}, b = {ld}; // 错误：转换未执行，因为存在丢失信息的危险
                         // 编译时控制台输入信息：warning: narrowing conversion of 'ld' from 'long double' to 'int'
    int c(ld), d = ld; // 正确：转换执行，且确实丢失了部分值

    // std::cin >> int input_value; // error: expected primary-expression before 'int'
    // double salary = wage = 9.99; //  error: 'wage' was not declared in this scope

    return 0;
}