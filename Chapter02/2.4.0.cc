#include "2.4.h"
#include <iostream>

extern const int mySize = 520; // 2.4.h头文件中定义了extern const int mySize;

int main()
{
    std::cout << mySize << std::endl;
    return 0;
}