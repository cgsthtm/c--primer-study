/**
 * @brief 2.5.1 类型别名
 * 类型别名（type alias）是一个名字，它是某种类型的同义词。
 * 使用类型别名有很多好处，它让复杂的类型名字变得简单明了、易于理解和使用，还有助于程序员清楚地知道使用该类型的真实目的。
 * @param argc 
 * @param argv 
 * @return int 
 */
#include "Chapter01/Sales_item.h"
int main(int argc, char const *argv[])
{
    //有两种方法可用于定义类型别名。
    // 1.传统的方法是使用关键字typedef
    // 2.新标准使用别名声明(关键字using)来定义类型的别名
    typedef double wages; // wages是double的同义词
    typedef wages base,*p; // base是double的同义词，p是double*的同义词
    using SI = Sales_item; // SI是Sales_item的同义词

    // 如果某个类型别名指代的是复合类型或常量，那么把它用到声明语句里就会产生意想不到的后果。
    typedef char *pstring;
    const pstring cstr = 0; // cstr是指向char的常量指针
    const pstring *ps; // ps是一个指针，它的对象是指向char的常量指针
    // 上述两条声明语句的基本数据类型都是const pstring，和过去一样，const是对给定类型的修饰。
    // pstring实际上是指向char的指针，因此，const pstring就是指向char的常量指针，而非指向常量字符的指针。

    return 0;
}
