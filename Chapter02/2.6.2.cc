/**
 * @file 2.6.2 使用Sales_data类
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2021-12-27
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <string>
#include "Sales_data.h"
int main(int argc, char const *argv[])
{
    Sales_data data1, data2;
    // 读入data1和data2的代码
    double price = 0;
    std::cin >> data1.bookNo >> data1.units_sold >> price;
    data1.revenue = data1.units_sold * price;
    std::cin >> data2.bookNo >> data2.units_sold >> price;
    data2.revenue = data2.units_sold * price;

    // 检查data1和data2的ISBN是否相同，如果相同，求data1和data2的总和
    if(data1.bookNo == data2.bookNo)
    {
        unsigned totalCnt = data1.units_sold + data2.units_sold;
        double totalRevenue = data1.revenue + data2.revenue;
        // 输出：ISBN、销售总量、总额、平均价格
        std::cout << data1.bookNo << " " << totalCnt << " " << totalRevenue << " ";
        if(totalCnt != 0)
        {
            std::cout << totalRevenue / totalCnt << std::endl;
        }
        else
        {
            std::cout << "(no sales)" << std::endl;
        }
        return 0;
    }
    else
    {
        std::cout << "Data must refer to the same ISBN" << std::endl;
        return -1;
    }
}
