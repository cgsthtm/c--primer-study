/**
 * @file 2.4.cc 
 * @author your name (you@domain.com)
 * @brief 2.4 const限定符
 * @version 0.1
 * @date 2021-12-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include "2.4.h" // 2.4.h头文件中定义了extern const int bufSize = 512;

extern const int mySize = 520; // 2.4.h头文件中定义了extern const int mySize;

int main()
{
    // 对于const变量不管是声明还是定义都添加extern关键字，这样只需定义一次就可以在多个文件中使用它了
    std::cout << bufSize << std::endl;

    // extern const int mySize = 520; // 函数体内部，不允许初始化extern修饰的变量 error: 'mySize' has both 'extern' and initializer

    return 0;
}