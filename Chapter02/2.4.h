// 对于const变量不管是声明还是定义都添加extern关键字，这样只需定义一次就可以在多个文件中使用它了
extern const int bufSize = 512;
extern const int mySize;