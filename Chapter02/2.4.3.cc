/**
 * @file 2.4.3.cc
 * @author your name (you@domain.com)
 * @brief 2.4.3 顶层const
 * 用顶层const表示指针本身是个常量，而用底层const表示指针所指的对象是一个常量
 * @version 0.1
 * @date 2021-12-25
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>

int main(int argc, char const *argv[])
{
    int i = 0;
    int *const p1 = &i; // 不能改变pi的值，这是一个顶层const
    const int ci = 42; // 不能改变ci的值，这是一个顶层const
    const int *p2 = &ci; // 允许改变p2的值，这是一个底层const
    const int *const p3 = p2; // 靠右的const是顶层const，靠左的是底层const
    const int &r = ci; // 用于声明引用的const都是底层const
    i = ci; // 正确，拷贝ci的值，ci是个顶层const，对此操作无影响
    p2 = p3; // 正确，p2和p3指向的对象类型相同，p3顶层const的部分不受影响

    // int *p = p3; // 错误，p3包含底层const的定义，而p没有
    p2 = p3; // 正确
    p2 = &i; // 正确，int*能转换成const int*
    // int &r = ci; // 错误，普通的int&不能绑定到int常量上
    const int &r2 = i; // 正确，const int&可以绑定到一个普通int上
    

    return 0;
}
