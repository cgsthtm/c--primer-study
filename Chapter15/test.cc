#include <cstdint>
#include <iostream>

using std::cout,std::endl;

char Data_test[] = {0,1,0,0,0,0,0,0};

uint8_t CalculateCRC8(void)
{
  uint8_t i,j,crc8,poly;
  crc8 = 0;
  poly=0x1D;
  for(i=1;i<8;i++)
  {
    crc8 ^= Data_test[i];
    for(j=0;j<8;j++)
    {
      if(crc8&0x80)
      {
        crc8 = (crc8 << 1)^poly;
      }
      else
      {
        crc8 <<= 1;
      }
    }
  }
  return crc8;
}

int main()
{
    uint8_t res = CalculateCRC8();
    cout << res << endl;
    cout << 1.123+1/100 << endl;
    int i = 1001;
    if(i/1000==0)
        cout << 0 << endl;
    else if(i/1000>0 && i/1400==0)
        cout << 1 << endl;
    else if(i/1400>0 && i/2200==0)
        cout << 2 << endl;
    else if(i/2200>0 && i/2800==0)
        cout << 3 << endl;
    else if(i/2800>0 && i/4800==0)
        cout << 4 << endl;
    else if(i/4800>0 && i/5400==0)
        cout << 5 << endl;
    else if(i/5400>0 && i/5600==0)
        cout << 6 << endl;
    else if(i/5600>0 && i/6000==0)
        cout << 7 << endl;
    else if(i/6000>0 && i/8500==0)
        cout << 8 << endl;
    else if(i/6000>0 && i/8500==0)
        cout << 9 << endl;
    else if(i/8500>0 && i/9500==0)
        cout << 10 << endl;
    else if(i/9500>0 && i/11700==0)
        cout << 11 << endl;
    else if(i/11700>0 && i/12200==0)
        cout << 12 << endl;
    else if(i/12200>0 && i/12400==0)
        cout << 13 << endl;
    else if(i/12400>0 && i/13200==0)
        cout << 14 << endl;
    else if(i/13200>0 && i/13400==0)
        cout << 15 << endl;
    else if(i/13400>0 && i/14200==0)
        cout << 16 << endl;
    else if(i/14200>0 && i/15500==0)
        cout << 17 << endl;
    else if(i/15500>0 && i/16200==0)
        cout << 18 << endl;
    else if(i/16200>0 && i/17500==0)
        cout << 19 << endl;
    else if(i/17500>0 && i/18700==0)
        cout << 20 << endl;
    else if(i/18700>0 && i/19500==0)
        cout << 21 << endl;
    else
        cout << 22 << endl;
    return 0;
}