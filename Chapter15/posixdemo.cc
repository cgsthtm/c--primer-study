#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <pthread.h> // 使用pthread_getattr_np()函数需要定义宏_GNU_SOURCE，而且要在pthread.h前定义
#include <stdio.h>
#include <stdlib.h>
#include "unistd.h" // for sleep
#include <errno.h>

/// 输出自定义的错误信息
#define handle_error_en(en,msg) do {errno=en;perror(msg);exit(EXIT_FAILURE);} while (0)
static void* thread_start7(void *arg)
{
    int i,s;
    pthread_attr_t gattr; // 定义线程属性结构体
    s = pthread_getattr_np(pthread_self(),&gattr); // 获取当前线程属性结构值
    if(s!=0)
        handle_error_en(s,"pthread_getattr_np"); // 打印错误信息
    printf("Thread's detachstate attribute\n");
    s = pthread_attr_getdetachstate(&gattr,&i); // 从属性结构值中获取分离状态的属性
    if(s)
        handle_error_en(s,"pthread_attr_getdetachstate");
    printf("Detach state =%s\n",
                    (i == PTHREAD_CREATE_DETACHED)?"PTHREAD_CREATE_DETACHED" :
                    (i == PTHREAD_CREATE_JOINABLE)?"PTHREAD_CREATE_JOINABLE" :
                    "???");
    pthread_attr_destroy(&gattr);

}

int main()
{

}