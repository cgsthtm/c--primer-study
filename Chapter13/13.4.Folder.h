#ifndef FOLDER_H
#define FOLDER_H

#include <string>
#include <set>
#include "13.4.Message.h"
using namespace std;

// TODO Folder类
class Folder
{
    friend class Message; // 友元类
public:
    void addMsg(Message *);
    void remMsg(Message *);
private:
    std::set<Message*> messages; // 本Folder中包含的Message
};



#endif // FOLDER_H