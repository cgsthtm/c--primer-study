// 13.1 拷贝、赋值与销毁
/**
 * 我们将以最基本的操作——拷贝构造函数、拷贝赋值运算符和析构函数作为开始。我们在13.6节（第470页）中将介绍移动操作（新标准所引入的操作）。
 */

#include <iterator>
#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <string>
#include <array>
#include <stack>
#include <queue>
#include <algorithm>
#include <numeric>
using std::swap;
using std::vector, std::list, std::deque, std::forward_list, std::string, std::array, std::stack, std::queue;
#include "../Chapter07/Sales_data.h"
#include <iostream>
using std::begin, std::cbegin, std::end, std::cend, std::find, std::accumulate, std::equal, std::fill, std::fill_n, std::back_inserter;
using std::cin, std::cout, std::endl;
using std::copy, std::replace, std::replace_copy;
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <memory>
#include <new>
using namespace std;

// 演示赋值运算符，赋值运算符通常应该返回一个指向其左侧运算对象的引用。
class Foo
{
public:
    Foo& operator=(const Foo&); // 赋值运算符
    Foo();            // 默认构造函数
    Foo(const Foo &); // 拷贝构造函数
};

int main()
{
    // 13.1.2 拷贝赋值运算符
    // 与类控制其对象如何初始化一样，类也可以控制其对象如何赋值：[插图]
    Sales_data trans,accum;
    trans = accum; // 使用Sales_data的拷贝赋值运算符，与拷贝构造函数一样，如果类未定义自己的拷贝赋值运算符，编译器会为它合成一个。

    // 重载赋值运算符
    // 重载运算符本质上是函数，其名字由operator关键字后接表示要定义的运算符的符号组成。因此，赋值运算符就是一个名为operator=的函数。
    // 重载运算符的参数表示运算符的运算对象。某些运算符，包括赋值运算符，必须定义为成员函数。
    // 如果一个运算符是一个成员函数，其左侧运算对象就绑定到隐式的this参数（参见7.1.2节，第231页）。
    Foo foo1,foo2;
    foo1 = foo2; // 赋值运算符通常应该返回一个指向其左侧运算对象的引用。

    // 合成拷贝赋值运算符
    // 与处理拷贝构造函数一样，如果一个类未定义自己的拷贝赋值运算符，编译器会为它生成一个合成拷贝赋值运算符（synthesized copy-assignment operator）。
    // 类似拷贝构造函数，对于某些类，合成拷贝赋值运算符用来禁止该类型对象的赋值（参见13.1.6节，第450页）。
    // 作为一个例子，下面的代码等价于Sales_data的合成拷贝赋值运算符：[插图]
    // Sales_data& Sales_data::operator=(const Sales_data &rhs)
    // {
    //     bookNo = rhs.bookNo;
    //     units_sold = rhs.units_sold;
    //     revenue = rhs.revenue;
    //     return *this;  // 返回一个此对象的引用（this是个常量指针，指向此对象本身，所以*this解引用该指针，即return *this返回该对象，函数返回值类型为Sales_data&，是个引用，所以返回一个该对象的引用）
    // }



    return 0;
}