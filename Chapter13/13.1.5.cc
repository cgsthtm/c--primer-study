// 13.1 拷贝、赋值与销毁
/**
 * 我们将以最基本的操作——拷贝构造函数、拷贝赋值运算符和析构函数作为开始。我们在13.6节（第470页）中将介绍移动操作（新标准所引入的操作）。
 */

#include <iterator>
#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <string>
#include <array>
#include <stack>
#include <queue>
#include <algorithm>
#include <numeric>
using std::swap;
using std::vector, std::list, std::deque, std::forward_list, std::string, std::array, std::stack, std::queue;
#include "../Chapter07/Sales_data.h"
#include <iostream>
using std::begin, std::cbegin, std::end, std::cend, std::find, std::accumulate, std::equal, std::fill, std::fill_n, std::back_inserter;
using std::cin, std::cout, std::endl;
using std::copy, std::replace, std::replace_copy;
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <memory>
#include <new>
using namespace std;

// 演示将拷贝控制成员定义为=default来显式地要求编译器生成合成的版本
class Foo
{
public:
    // 拷贝控制成员：使用default
    Foo() = default;             // 默认构造函数
    Foo(const Foo &) = default;  // 拷贝构造函数
    Foo &operator=(const Foo &); // 赋值运算符
    ~Foo() = default;            // 析构函数
};
Foo &Foo::operator=(const Foo &) = default; // 在类外定义合成的非内联的拷贝赋值运算符

int main()
{
    // 13.1.5 使用=default
    // 我们可以通过将拷贝控制成员定义为=default来显式地要求编译器生成合成的版本
    // 当我们在类内用=default修饰成员的声明时，合成的函数将隐式地声明为内联的
    // 如果我们不希望合成的成员是内联函数，应该只对成员的类外定义使用=default，就像对拷贝赋值运算符所做的那样。
    // 我们只能对具有合成版本的成员函数使用=default（即，默认构造函数或拷贝控制成员）。

    return 0;
}