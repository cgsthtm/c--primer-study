/**
 * @brief 5.6 try语句块和异常处理
 */
#include <iostream>
#include <exception> // 定义了最通用的异常类exception。它只报告异常的发生，不提供任何额外信息。
#include <stdexcept> // 定义了几种常用的异常类
#include <new>       // 定义了bad_alloc异常类型
#include <typeinfo>  // 定义了bad_cast异常类型
using std::runtime_error;

int main(int argc, char const *argv[])
{
    // throw表达式
    if(true)
        throw runtime_error("throw rumtime_error exception");

    // try语句块
    try
    {
        // 
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
    // 标准异常
    // C++标准库定义了一组类，用于报告标准库函数遇到的问题。这些异常类也可以在用户编写的程序中使用，它们分别定义在4个头文件中：
    // 1. exception头文件定义了最通用的异常类exception。它只报告异常的发生，不提供任何额外信息。
    // 2. stdexcept头文件定义了几种常用的异常类，详细信息在表5.1中列出。
    // 3. new头文件定义了bad_alloc异常类型，这种类型将在12.1.2节（第407页）详细介绍。
    // 4. type_info头文件定义了bad_cast异常类型，这种类型将在19.2节（第731页）详细介绍。

    return 0;
}
