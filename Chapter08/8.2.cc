// 8.2 文件输入输出
/**
 * 头文件fstream定义了三个类型来支持文件IO：
 * 1. ifstream从一个给定文件读取数据
 * 2. ofstream向一个给定文件写入数据
 * 3. 以及fstream可以读写给定文件。
 */

#include <iostream>
#include <fstream>
using std::cerr, std::clog;
using std::cin, std::cout, std::endl, std::flush, std::ends, std::unitbuf, std::nounitbuf;
using std::ifstream, std::ofstream;
using std::ostream, std::istream;
using std::string;

int main()
{
    // 8.2.1 使用文件流对象
    /*
      当我们想要读写一个文件时，可以定义一个文件流对象，并将对象与文件关联起来。
      每个文件流类都定义了一个名为open的成员函数，它完成一些系统相关的操作，来定位给定的文件，并视情况打开为读或写模式。
      创建文件流对象时，我们可以提供文件名（可选的）。如果提供了一个文件名，则open会自动被调用。
    */
    string ifile = "D:\\MyStudy\\C++\\C++Primer5\\Chapter08\\a";
    ifstream in(ifile);        // 构造一个ifstream并打开给定的文件
    ofstream out;              // 输出文件流未关联到任何文件
    out.open(ifile + ".copy"); // 打开指定文件
    if (out)                   // 检查open是否成功
    {
    }
    in.close();           // 关闭文件
    in.open(ifile + "2"); // 打开另一个文件
                          // 当一个fstream对象被销毁时，close会自动被调用。

    // 8.2.2 文件模式
    /*
      每个流都有一个关联的文件模式（file mode），用来指出如何使用文件。表8.4列出了文件模式和它们的含义。
      文件模式：
      1. in（读方式打开）
      2. out（写方式打开）
      3. app（每次写操作前均定位到文件末尾）
      4. ate（打开文件后立即定位到文件末尾）
      5. trunc（截断文件）
      6. binary（以二进制方式进行IO）
      与ifstream关联的文件默认以in模式打开；与ofstream关联的文件默认以out模式打开；与fstream关联的文件默认以in和out模式打开。
    */
    // 在这几条语句中file1都被截断
    ofstream out1("file1");                // 隐含以输出模式打开文件并截断文件
    ofstream out2("file1", ofstream::out); // 隐含的截断文件
    ofstream out3("file1", ofstream::out | ofstream::trunc);
    // 为了保留文件内容，我们必须显示指定app模式
    ofstream app1("file2", ofstream::app); // 隐含为输出模式
    ofstream app2("file2", ofstream::out | ofstream::app);
    ofstream outa; // 未执行文件打开模式
    outa.open("filea"); // 模式隐含设置为输出和截断
    outa.close();
    outa.open("filea",ofstream::app); // 模式为输出和追加
    outa.close();


    return 0;
}