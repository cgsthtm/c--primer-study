/*A.1　标准库名字和头文件
  本书中大多数代码没有给出编译程序所需的实际#include指令。为了方便读者，表A.1列出了本书程序用到的标准库名字以及它们所在的头文件。
表A.1：标准库名字和头文件
名字                    头文件
abort                   <cstdlib>
accumulate              <numeric>
allocator               <memory>
array                   <array>
auto_ptr                <memory>
back_inserter           <iterator>
bad_alloc               <new>
bad_array_new_length    <new>
bad_cast                <typeinfo>
begin                   <iterator>
bernoulli_distribution  <random>
bind                    <functional>
bitset                  <bitset>
boolalpha               <iostream>
cerr                    <iostream>
cin                     <iostream>
cmatch                  <regex>
copy                    <algorithm>
count                   <algorithm>
count_if                <algorithm>
cout                    <iostream>
cref                    <functional>
csub_match              <regex>
dec                     <iostream>
default_float_engine    <iostream>
default_random_engine   <random>
deque                   <deque>
domain _error           <stdexcept>
end                     <iterator>
endl                    <iostream>
ends                    <iostream>
equal_range             <algorithm>
exception               <exception>
fil1                    <algorithm>
fill_n                  <algorithm>
find                    <algorithm>
find_end                <algorithm>
find_first_of           <algorithm>
find_if                 <algorithm>
fixed                   <iostream>
flush                   <iostream>
for_each                <algorithm>
forward                 <utility>
forward_list            <forward_list>
free                    <cstdlib>
front_inserter          <iterator>
fstream                 <fstream>
function                <functional>
get                     <tuple>
getline                 <string>
greater                 <functional>
hash                    <functional>
hex                     <iostream>
hexfloat                <iostream>
ifstream                <fstream>
initializer_list        <initializer_list>
inserter                <iterator>
internal                <iostream>
ios_base                <ios_base>
isalpha                 <cctype>
islower                 <cctype>
isprint                 <cctype>
ispunct                 <cctype>
isspace                 <cctype>
istream                 <iostream>
istream_iterator        <iterator>
istringetream           <sstream>
isupper                 <cctype>
left                    <iostream>
less                    <functional>
less_equal              <functional>
1ist                    <list>
logic_error             <stdexcept>
lower_bound             <algorithm>
lround                  <cmath>
make_move_iterator      <iterator>
make_pair               <utility>
make_shared<memory>
make_tuple<tuple>
mal1cc<estdlib>
map<map>
max<algorithm>
max_element<algorithm>
mem_fn<functional>
mincalgorithm>
move<utility>
multimap<map>
multiset<set>
negate<functional>
noboolalpha<iostream>
normal_distribution<random>
noshowbase<iostream>
noshowpoint<iostream>
noskipws<iostream>
not1<functional>
nothrow<new>
nothrow_t<new>
nounitbuf<iostream>
nouppercase<iostream>
nth_element<algorithm>
oct<iostream>
ofstream<fstxeam>
ostream<iostream>
ostream_iterator<iterator>
ostringstream<sstream>
out_of_range<stdexcept>
pair<utility>
partial_sort<algorithm>
placeholders<functional>
placeholders::_1<functional>
plus<functional>
priority_queue<queue>
ptrdiff_t<cstddef>
queue<queue>
rand<random>
random_device<random>
range_error<stdexcept>
ref<functional>
regex<regex>
regex_constants<regex>
regex_error<regex>
regex_match<regex>
regex_replace<regex>
regex_search<regex>
remove_pointer<type_traits>
remove_reference<type_traits>
replace<algorithm>
replace_copy<algorithm>
reverse_iterator<iterator>
right<iostream>
runtime_error<stdexcept>
scientific<iostream>
set<set>
set_difference<algorithm>
set_intersection<algorithm>
set_union<algorithm>
setfill<iomanip>
setprecision<iomanip>
setw<iomanip>
shared_ptr<memory>
showbase<iostream>
showpoint<iostream>
size_t<cstddef>
skipws<iostream>
smatch<regex>
sort<algorithm>
sqrt<cmath>
sregex_iterator<regex>
ssub_match<regex>
stable_sort<algorithm>
stack<stack>
stoi<string>
strcmp<cstring>
strcpy<cstring>
string<string>
stringstream<sstream>
strlen<cstring>
strncpy<cstring>
strtod<string>
swap<utility>
terminate<exception>
time<ctime>
tolower<cctype>
toupper<cctype>
transform<algorithm>
tuple<tuple>
tuple_element<tuple>
tuple_size<tuple>
type_info<typeinfo>
unexpected<exception>
uniform_int_distribution<random>
uniform_real_distribution<random>
uninitialized_copy<memory>
uninitialized_fill<memory>
unique<algorithm>
unique_copy<algorithm>
unique_ptr<memory>
unitbuf<iostream>
unordered_map<unordered_map>
unordered_multimap<unordered_map>
unordered_multiset<unordered_set>
unordered_set<unordered set>
upper_bound<algorithm>
uppercase<iostream>v
ector<vector>
weak_ptr<memory>
*/