/*A.3 随机数
  标准库定义了一组随机数引擎类和适配器，使用不同数学方法生成伪随机数。标准库还定义了一组分布模板，根据不同的概率分布生成随机数。引擎和分布类型的名字都与它们的数学性质相对应。
    这些类如何生成随机数的细节已经大大超出了本书的范围。在本节中，我们将列出这些引擎和分布类型，但读者需要查询其他资料来学习如何使用这些类型。
*/

// A.3.1 随机数分布
/*除了总是生成bool类型的bernoulli_distribution外，其他分布类型都是模板。每个模板都接受单个类型参数，它指出了分布生成的结果类型。
均匀分布
uniform_int_distribution<IntT>u(m,n);
uniform_real_distribution<RealT>u(x,y);
伯努利分布
bernoulli_distribution b(p);
binomial_distribution<IntT>b(t,p);
geometric_distribution<IntT>g(p);
negative_binomial_distribution<IntT>nb(k,p);
泊松分布
poisson_distribution<IntT>p(x);
exponential_distribution<RealT>e(lam);
gamma_distribution<RealT>g(a,b);
weibull_distribution<RealT>w(a,b);
extreme_value_distribution<RealT>e(a,b);
正态分布
normal_distribution<RealT>n(m,s);
lognormal_distribution<RealT>ln(m,s);
chi_squared_distribution<RealT>c(x);
cauchy_distribution<RealT>c(a,b);
fisher_f_distribution<RealT>f(m,n);
student_t_distribution<RealT>s(n);
抽样分布
discrete_distribution<IntT>d(i,j);
discrete_distribution<IntT>d{il};
piecewise_constant_distribution<RealT>pc(b,e,w);
piecewise_linear_distribution<RealT>pl(b,e,w);

*/

// A.3.2 随机数引擎
/*标准库还定义了三个适配器，可以修改给定引擎生成的序列。引擎和引擎适配器类都是模板。与分布的参数不同，这些引擎的参数更为复杂，且需深入了解特定引擎使用的数学知识。
  标准库还定义了几个从引擎和适配器类型构造的类型。default_random_engine类型是一个参数化的引擎类型的类型别名，参数化所用的变量的目的是在通常情况下获得好的性能。
    标准库还定义了几个类，它们都是一个引擎或适配器的完全特例化版本。标准库定义的引擎和特例化版本如下：
default_random_engine
  某个其他引擎类型的类型别名，目的是用于大多数情况。
linear_congruential_engine
  minstd_rand0的乘数为16807，模为2147483647，增量为0。minstd_rand的乘数为48271，模为2147483647，增量为0。
mersenne_twister_engine
  mt19937为32位无符号梅森旋转生成器。mt19937_64为64位无符号梅森旋转生成器。
subtract_with_carry_engine
  ranlux24_base为32位无符号借位减法生成器。ranlux48_base为64位无符号借位减法生成器。
discard_block_engine
  引擎适配器，将其底层引擎的结果丢弃。用要使用的底层引擎、块大小和旧块大小来参数化。ranlux24使用ranlux24_base引擎，块大小为223，旧块大小为23。
  ranlux48使用ranlux48_base引擎，块大小为389，旧块大小为11。
independent_bits_engine
  引擎适配器，生成指定位数的随机数。用要使用的底层引擎、结果的位数以及保存生成的二进制位的无符号整型类型来参数化。指定的位数必须小于指定的无符号类型所能保存的位数。
shuffle_order_engine
  引擎适配器，返回的就是底层引擎生成的数，但返回的顺序不同。用要使用的底层引擎和要混洗的元素数目来参数化。knuth_b使用minstd_rand0和表大小256。
*/