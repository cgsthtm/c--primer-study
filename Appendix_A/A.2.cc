/*A.2　算法概览
  标准库定义了超过100个算法。要想高效使用这些算法需要了解它们的结构而不是单纯记忆每个算法的细节。
  在本节中，我们将简要描述每个算法，在下面的描述中，
    · beg和end是表示元素范围的迭代器（参见9.2.1节，第296页）。几乎所有算法都对一个由beg和end表示的序列进行操作。
    · beg2是表示第二个输入序列开始位置的迭代器。end2表示第二个序列的末尾位置（如果有的话）。如果没有end2，则假定beg2表示的序列与beg和end表示的序列一样大。
      beg和beg2的类型不必匹配，但是，必须保证对两个序列中的元素都可以执行特定操作或调用给定的可调用对象。
    · dest是表示目的序列的迭代器。对于给定输入序列，算法需要生成多少元素，目的序列必须保证能保存同样多的元素。
    · unaryPred和binaryPred是一元和二元谓词（参见10.3.1节，第344页），分别接受一个和两个参数，都是来自输入序列的元素，两个谓词都返回可用作条件的类型。
    · comp是一个二元谓词，满足关联容器中对关键字序的要求（参见11.2.2节，第378页）。
    · unaryOp和binaryOp是可调用对象（参见10.3.2节，第346页），可分别使用来自输入序列的一个和两个实参来调用。
*/

// A.2.1 查找对象的算法
/*
简单查找算法
find(beg,end,val)
find_if(beg,end,unaryPred)
find_if_not(beg,end,unaryPred)
count(beg,end,val)
count_if(beg,end,unaryPred)
all_of(beg,end,unaryPred)
any_of(beg,end,unaryPred)
none_of(beg,end,unaryPred)
查找重复值的算法
adjacent_find(beg,end)
adjacent_find(beg,end,binaryPred)
search_n(beg,end,count,val)
search_n(beg,end,count,val,binaryPred)
查找子序列的算法
search(beg1,end1,beg2,end2)
search(beg1,end1,beg2,end2,binaryPred)
find_first_of(beg1,end1,beg2,end2)
find_first_of(beg1,end1,beg2,end2,binaryPred)
find_end(beg1,end1,beg2,end2)
find_end(beg1,end1,beg2,end2,binaryPred)
*/

// A.2.2 其他只读算法
/*
for_each(beg,end,unaryOp)
mismatch(beg1,end1,beg2)
mismatch(beg1,end1,beg2,binaryPred)
equal(beg1,end1,beg2)
equal(beg1,end1,beg2,binaryPred)
*/

// A.2.3 二分搜索算法
/*
lower_bound(beg,end,val)
lower_bound(beg,end,val,comp)
upper_bound(beg,end,val)
upper_bound(beg,end,val,comp)
equal_range(beg,end,val)
equal_range(beg,end,val,comp)
binary_search(beg,end,val)
binary_search(beg,end,val,comp)
*/

// A.2.4 写容器元素的算法
/*
只写不读元素的算法
fill(beg,end,val)
fill_n(dest,cnt,val)
generate(beg,end,Gen)
generate_n(dest,cnt,Gen)
使用输入迭代器的写算法
copy(beg,end,dest)
copy_if(beg,end,dest,unaryPred)
copy_n(beg,n,dest)
move(beg,end,dest)
transform(beg,end,dest,unaryOp)
transform(beg,end,beg2,dest,binaryOp)
replace_copy(beg,end,dest,old_val,new_val)
replace_copy_if(beg,end,dest,unaryPred,new_val)
merge(beg1,end1,beg2,end2,dest)
merge(beg1,end1,beg2,end2,dest,comp)
使用前向迭代器的写算法
iter_swap(iter1,iter2)
swap_ranges(beg1,end1,beg2)
replace(beg,end,old_val,new_val)
replace_if(beg,end,unaryPred,new_val)
使用双向迭代器的写算法
copy_backward(beg,end,dest)
move_backward(beg,end,dest)
inplace_merge(beg,mid,end)
inplace_merge(beg,mid,end,comp)
*/

// A.2.5 划分与排序算法
/*
划分算法
is_partitioned(beg,end,unaryPred)
partition_copy(beg,end,dest1,dest2,unaryPred)
partition_point(beg,end,unaryPred)
stable_partition(beg,end,unaryPred)
partition(beg,end,unaryPred)
排序算法
sort(beg,end)
stable_sort(beg,end)
sort(beg,end,comp)
stable_sort(beg,end,comp)
is_sorted(beg,end)
is_sorted(beg,end,comp)
is_sorted_until(beg,end)
is_sorted_until(beg,end,comp)
partial_sort(beg,mid,end)
partial_sort(beg,mid,end,comp)
partial_sort_copy(beg,end,destBeg,destEnd)
partial_sort_copy(beg,end,destBeg,destEnd,comp)
nth_element(beg,nth,end)
nth_element(beg,nth,end,comp)
*/

// A.2.6 通用重排操作
/*
使用前向迭代器的重排算法
remove(beg,end,val)
remove_if(beg,end,unaryPred)
remove_copy(beg,end,dest,val)
remove_copy_if(beg,end,dest,unaryPred)
unique(beg,end)
unique(beg,end,binaryPred)
unique_copy(beg,end,dest)
unique_copy_if(beg,end,dest,binaryPred)
rotate(beg,mid,end)
rotate_copy(beg,mid,end,dest)
使用双向迭代器的重排算法
reverse(beg,end)
reverse_copy(beg,end,dest)
使用随机访问迭代器的重排算法
random_shuffle(beg,end)
random_shuffle(beg,end,rand)
shuffle(beg,end,Uniform_rand)
*/

// A.2.7 排列算法
/*
is_permutation(beg1,end1,beg2)
is_permutation(beg1,end1,beg2,binaryPred)
next_permutation(beg,end)
next_permutation(beg,end,comp)
prev_permutation(beg,end)
prev_permutation(beg,end,comp)
*/

// A.2.8 有序序列的集合算法
/*
includes(beg,end,beg2,end2)
includes(beg,end,beg2,end2,comp)
set_union(beg,end,beg2,end2,dest)
set_union(beg,end,beg2,end2,dest,comp)
set_intersection(beg,end,beg2,end2,dest)
set_intersection(beg,end,beg2,end2,dest,comp)
set_difference(beg,end,beg2,end2,dest)
set_difference(beg,end,beg2,end2,dest,comp)
set_symmetric_difference(beg,end,beg2,end2,dest)
set_symmetric_difference(beg,end,beg2,end2,dest,comp)
*/

// A.2.9 最小值和最大值
/*
min(val1,val2)
min(val1,val2,comp)
min(init_list)
min(init_list,comp)
max(val1,val2)
max(val1,val2,comp)
max(init_list)
max(init_list,comp)
minmax(val1,val2)
minmax(val1,val2,comp)
minmax(init_list)
minmax(init_list,comp)
min_element(beg,end)
min_element(beg,end,comp)
max_element(beg,end)
max_element(beg,end,comp)
minmax_element(beg,end)
minmax_element(beg,end,comp)
字典序比较
lexicographical_compare(beg1,end1,beg2,end2)
lexicographical_compare(beg1,end1,beg2,end2,comp)
*/

// A.2.10 数值算法
/*
accumulate(beg,end,init)
accumulate(beg,end,init,binaryOp)
inner_product(beg1,end1,beg2,init)
inner_product(beg1,end1,beg2,init,binOp1,binOp2)
partial_sum(beg,end,dest)
partial_sum(beg,end,dest,binaryOp)
adjacent_difference(beg,end,dest)
adjacent_difference(beg,end,dest,binaryOp)
iota(beg,end,val)
*/