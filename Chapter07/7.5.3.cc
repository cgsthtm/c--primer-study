// 7.5 构造函数再探
/**
 * 7.5.3 默认构造函数的作用
 * 在实际中，如果定义了其他构造函数，那么最好也提供一个默认构造函数。
 */

#include <iostream>
#include <vector>
#include "Sales_data.h"
//#include "Screen.h" // 难道是因为Window_mgr被声明成Screen的友元的原因？导致这个源文件同时包含这两个头文件时会导致一些问题？
#include "WindowMgr.h"
using std::cerr, std::clog;
using std::cin, std::cout, std::endl;

class NoDefault
{
public:
    NoDefault(const string&) {}

    // 还有其他成员但没有其他构造函数了
};

struct A
{
    NoDefault my_mem; // 默认情况下my_mem是public的
};

A a; // 错误，不能为A合成构造函数

struct B
{
    B() {} // 错误，b_member没有初始值（此处应该用构造函数初始值列表初始化b_member）
    NoDefault b_member;
};

Sales_data obj(); // 正确，定义了一个函数而非对象，obj()函数返回Sales_data对象
Sales_data obj1; // 正确，obj1是个默认初始化的对象


int main()
{
    if(obj.isbn() == "XXXXXXXXXXXX" ) // 错误，obj是个函数，不是对象
    {

    }
    if(obj1.isbn() == "XXXXXXXXXXXX" ) // 正确，obj1是个对象，调用了isbn()成员函数
    return 0;
}