// 7.5 构造函数再探
/**
 * 7.5.2 委托构造函数
 * 一个委托构造函数使用它所属类的其他构造函数执行它自己的初始化过程，或者说它把它自己的一些（或者全部）职责委托给了其他构造函数。
 * 当一个构造函数委托给另一个构造函数时，受委托的构造函数的初始值列表和函数体被依次执行。
 * 在Sales_data类中，受委托的构造函数体恰好是空的。假如函数体包含有代码的话，将先执行这些代码，然后控制权才会交还给委托者的函数体。
 */

#include <iostream>
#include <vector>
#include "Sales_data.h"
//#include "Screen.h" // 难道是因为Window_mgr被声明成Screen的友元的原因？导致这个源文件同时包含这两个头文件时会导致一些问题？
#include "WindowMgr.h"
using std::cerr, std::clog;
using std::cin, std::cout, std::endl;

class ConstRef
{
public:
    // 非委托构造函数使用对应的实参初始化成员
    ConstRef(int ii) : i(ii), ci(ii), ri(i) {}
    // 委托构造函数，下面的构造函数委托给上面的构造函数
    ConstRef() : ConstRef(6) {}

private:
    int i;
    const int ci;
    int &ri;
};

int main()
{

    return 0;
}