// 7.2 访问控制与封装
/**
 * 在C++语言中，我们使用访问说明符（access specifiers）加强类的封装性，如：private、public
 * 
 * 作为接口的一部分，构造函数和部分成员函数（即isbn和combine）紧跟在public说明符之后；
 * 而数据成员和作为实现部分的函数则跟在private说明符后面。
 */