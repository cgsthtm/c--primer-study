// 7.3 类的其他特性
// 这些特性包括：类型成员、类的成员的类内初始值、可变数据成员、内联成员函数、从成员函数返回＊this
// 关于如何定义并使用类类型及友元类的更多知识。

// 7.3.3 类类型
/**
 * 我们可以把类名作为类型的名字使用，从而直接指向类类型。或者，我们也可以把类名跟在关键字class或struct后面。
 * 就像可以把函数的声明和定义分离开来一样（参见6.1.2节，第186页），我们也能仅仅声明类而暂时不定义它。
 * 一旦一个类的名字出现后，它就被认为是声明过了（但尚未定义），因此类允许包含指向它自身类型的引用或指针。
 */

#include <iostream>
#include <vector>
#include "Sales_data.h"
using std::cerr, std::clog;
using std::cin, std::cout, std::endl;

// Screen类的声明
class Screen; // 这种声明有时被称作前向声明（forward declaration）

// 一旦一个类的名字出现后，它就被认为是声明过了（但尚未定义），因此类允许包含指向它自身类型的引用或指针
class Link_screen{
    //Screen window; // 错误，此时只被声明未被定义的Screen是不完全类型
    Link_screen *next;
    Link_screen *prev;
};

int main()
{
    // 我们可以把类名作为类型的名字使用，从而直接指向类类型。或者，我们也可以把类名跟在关键字class或struct后面
    Sales_data item1;
    class Sales_data item1; // 一条等价的声明。这种方式从C语言继承而来，并且在C++语言中也是合法的
    
    return 0;
}