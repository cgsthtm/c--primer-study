// 7.5 构造函数再探
/**
 * 7.5.5 聚合类
 * 聚合类（aggregate class）使得用户可以直接访问其成员，并且具有特殊的初始化语法形式。
 * 当一个类满足如下条件时，我们说它是聚合的：
 * 1. 所有成员都是public的。
 * 2. 没有定义任何构造函数。
 * 3. 没有类内初始值（参见2.6.1节，第64页）。
 * 4. 没有基类，也没有virtual函数，关于这部分知识我们将在第15章详细介绍
 */

#include <iostream>
#include <vector>
#include "Sales_data.h"
//#include "Screen.h" // 难道是因为Window_mgr被声明成Screen的友元的原因？导致这个源文件同时包含这两个头文件时会导致一些问题？
#include "WindowMgr.h"
using std::cerr, std::clog;
using std::cin, std::cout, std::endl;

// 这是一个聚合类
struct Data
{
    int iVal;
    string s;
};

// 使用花括号括起来的成员初始值列表，来初始化聚合类的数据成员
// 如果初始值列表中的元素个数少于类的成员数量，则靠后的成员被值初始化（参见3.5.1节，第101页）。
Data val1 = {0,"Anna"};
Data val1 = {0}; // 数据成员s被值初始化为空字符串""

// 值得注意的是，显式地初始化类的对象的成员存在三个明显的缺点：
// 1. 要求类的所有成员都是public的。
// 2. 将正确初始化每个对象的每个成员的重任交给了类的用户（而非类的作者）。
//    因为用户很容易忘掉某个初始值，或者提供一个不恰当的初始值，所以这样的初始化过程冗长乏味且容易出错。
// 3. 添加或删除一个成员之后，所有的初始化语句都需要更新。



int main()
{
    return 0;
}