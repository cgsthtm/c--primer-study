// 7.5 构造函数再探
/**
 * 7.5.6 字面值常量类
 * 在6.5.2节（第214页）中我们提到过constexpr函数的参数和返回值必须是字面值类型。
 * 除了算术类型、引用和指针外，某些类也是字面值类型。
 * 数据成员都是字面值类型的聚合类（参见7.5.5节，第266页）是字面值常量类。
 * 如果一个类不是聚合类，但它符合下述要求，则它也是一个字面值常量类：
 * 1. 数据成员都必须是字面值类型。
 * 2. 类必须至少含有一个constexpr构造函数。
 * 3. 如果一个数据成员含有类内初始值，则内置类型成员的初始值必须是一条常量表达式（参见2.4.4节，第58页）；
 *    或者如果成员属于某种类类型，则初始值必须使用成员自己的constexpr构造函数。
 * 4. 类必须使用析构函数的默认定义，该成员负责销毁类的对象（参见7.1.5节，第239页）。
 */

#include <iostream>
#include <vector>
#include "Sales_data.h"
//#include "Screen.h" // 难道是因为Window_mgr被声明成Screen的友元的原因？导致这个源文件同时包含这两个头文件时会导致一些问题？
#include "WindowMgr.h"
using std::cerr, std::clog;
using std::cin, std::cout, std::endl;

// 字面值常量类
// constexpr构造函数必须初始化所有数据成员，初始值或者使用constexpr构造函数，或者是一条常量表达式。
class Debug
{
public:
    // constexpr构造函数体一般来说应该是空的。
    constexpr Debug(bool b = true) : hw(b), io(b), other(b) {}
    constexpr Debug(bool h, bool i, bool o) : hw(h), io(i), other(o) {}
    constexpr bool any() { return hw || io || other; }
    void set_io(bool b) { io = b; }
    void set_hw(bool b) { hw = b; }
    void set_other(bool b) { other = b; }

private:
    bool hw; // 硬件错误
    bool io; // IO错误
    bool other; // 其他错误
};

int main()
{
    return 0;
}