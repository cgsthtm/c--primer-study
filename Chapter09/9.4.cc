// 9.4 vector对象是如何增长的
/**
 * 为了支持快速随机访问，vector将元素连续存储——每个元素紧挨着前一个元素存储。
 *
 * 准库实现者采用了可以减少容器空间重新分配次数的策略。
 * 当不得不获取新的内存空间时，vector和string的实现通常会分配比新的空间需求更大的内存空间。
 * 容器预留这些空间作为备用，可用来保存更多的新元素。这样，就不需要每次添加新元素都重新分配容器的内存空间了。
 */

#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <string>
#include <array>
using std::swap;
using std::vector, std::list, std::deque, std::forward_list, std::string, std::array;
#include "../Chapter07/Sales_data.h"
#include <iostream>
using std::cin, std::cout, std::endl;

int main()
{
    // 管理容量的成员函数
    // capacity操作告诉我们容器在不扩张内存空间的情况下可以容纳多少个元素。reserve操作允许我们通知容器它应该准备保存多少个元素。
    // shrink_to_fit只适用于vector、string和deque
    // capacity和reserve只适用于vector和string
    vector<int> vi = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    vi.shrink_to_fit(); // 请将capacity()减少为与size()相同大小
    vi.capacity();      // 不重新分配内存空间的话，c可以保存多少元素
    vi.reserve(20);     // 分配至少能容纳n个元素的内存空间

    // capacity和size
    // 理解capacity和size的区别非常重要。容器的size是指它已经保存的元素的数目；
    // 而capacity则是在不分配新的内存空间的前提下它最多可以保存多少元素。
    vector<int> ivec;
    // size应该为0；capacity的值依赖于具体实现
    cout << " ivec:size:" << ivec.size() << " capacity:"<< ivec.capacity() << endl;
    // 向ivec添加24个元素
    for (vector<int>::size_type i = 0; i !=24; ++i)
        ivec.push_back(i);
    // size应该为24；capacity应该大于等于24，具体值依赖于标准库实现
    cout << " ivec:size:" << ivec.size() << " capacity:"<< ivec.capacity() << endl;
    ivec.reserve(50); // 将capacity至少设定为50，可能会更大
    // size应该为24；capacity应该大于等于50，具体值依赖于标准库实现
    cout << " ivec:size:" << ivec.size() << " capacity:"<< ivec.capacity() << endl;

    return 0;
}