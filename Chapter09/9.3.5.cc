// 9.3 顺序容器操作
/**
 * 顺序容器和关联容器的不同之处在于两者组织元素的方式。这些不同之处直接关系到了元素如何存储、访问、添加以及删除。
 *
 * 9.3.5 改变容器大小
 * 如表9.9所描述，我们可以用resize来增大或缩小容器，与往常一样，array不支持resize。
 * 如果当前大小大于所要求的大小，容器后部的元素会被删除；如果当前大小小于新大小，会将新元素添加到容器后部。
 */

#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <string>
#include <array>
using std::swap;
using std::vector, std::list, std::deque, std::forward_list, std::string, std::array;
#include "../Chapter07/Sales_data.h"
#include <iostream>
using std::cin, std::cout, std::endl;

int main()
{
    // 改变容器大小
    list<int> ilist(10, 42); // 10个int，每个都是42
    ilist.resize(15);        // 将5个值为0的元素添加到ilist的末尾
    ilist.resize(25, -1);    // 将10个值为-1的元素添加到ilist的末尾
    ilist.resize(5);         // 从ilist末尾删除20个元素

    return 0;
}