// 9.1 顺序容器概述
/**
 * 所有顺序容器都提供了快速顺序访问元素的能力，顺序容器类型：
 * 1. vector        可变大小数组。支持快速随机访问。在尾部之外的位置插入或删除元素可能很慢。
 * 2. deque         双端队列。支持快速随机访问。在头尾位置插入/删除速度很快。
 * 3. list          双向链表。只支持双向顺序访问。在任何位置插入/删除速度都很快。
 * 4. forward_list  单向链表。只支持单项顺序访问。在任何位置插入/删除速度都很快。
 * 5. array         固定大小数组。支持快速随机访问。不能添加或删除元素。
 * 6. string        与vector类似的容器，但专门用于保存字符。随机访问快。在尾部插入/删除速度快。
 * 例如，string和vector将元素保存在连续的内存空间中。由于元素是连续存储的，由元素的下标来计算其地址是非常快速的。
 * 但是，在这两种容器的中间位置添加或删除元素就会非常耗时：在一次插入或删除操作后，需要移动插入/删除位置之后的所有元素，来保持连续存储。
 * list和forward_list两个容器的设计目的是令容器任何位置的添加和删除操作都很快速。
 * 作为代价，list和forward_list这两个容器不支持元素的随机访问：为了访问一个元素，我们只能遍历整个容器。
 * 而且，与vector、deque和array相比，这两个容器的额外内存开销也很大。
 * deque是一个更为复杂的数据结构。与string和vector类似，deque支持快速的随机访问。
 * 与string和vector一样，在deque的中间位置添加或删除元素的代价（可能）很高。
 * 但是，在deque的两端添加或删除元素都是很快的，与list或forward_list添加删除元素的速度相当。
 * 
 * 如果你不确定应该使用哪种容器，那么可以在程序中只使用vector和list公共的操作：使用迭代器，不使用下标操作，避免随机访问。
 * 这样，在必要时选择使用vector或list都很方便。
 */

#include <vector>
#include <iostream>
#include <sstream>
using std::cerr, std::clog;
using std::cin, std::cout, std::endl, std::flush, std::ends, std::unitbuf, std::nounitbuf;
using std::istringstream, std::ostringstream;
using std::ostream, std::istream;
using std::string, std::vector;

int main()
{
    
    return 0;
}