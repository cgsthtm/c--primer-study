// 9.3 顺序容器操作
/**
 * 顺序容器和关联容器的不同之处在于两者组织元素的方式。这些不同之处直接关系到了元素如何存储、访问、添加以及删除。
 *
 * 9.3.4 特殊的forward_list操作
 * 在一个forward_list中添加或删除元素的操作是通过改变给定元素之后的元素来完成的。这样，我们总是可以访问到被添加或删除操作所影响的元素。
 * forward_list并未定义insert、emplace和erase，而是定义了名为insert_after、emplace_after和erase_after的操作（参见表9.8）。
 * 为了支持这些操作，forward_list也定义了before_begin，它返回一个首前（off-the-beginning）迭代器。
 * 这个迭代器允许我们在链表首元素之前并不存在的元素“之后”添加或删除元素（亦即在链表首元素之前添加删除元素）。
 */

#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <string>
#include <array>
using std::swap;
using std::vector, std::list, std::deque, std::forward_list, std::string, std::array;
#include "../Chapter07/Sales_data.h"
#include <iostream>
using std::cin, std::cout, std::endl;

int main()
{
    // 当在forward_list中添加或删除元素时，我们必须关注两个迭代器——一个指向我们要处理的元素，另一个指向其前驱。
    forward_list<int> flst = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    auto prev = flst.before_begin(); // 表示flst的“首前元素”
    auto curr = flst.begin();        // 表示flst中的第一个元素
    while (curr != flst.end())       // 仍有元素要处理
    {
        if (*curr % 2)                     // 若元素为奇数
            curr = flst.erase_after(prev); // 删除它并移动curr
        else
            prev = curr; // 移动迭代器curr指向下一个元素，prev指向curr移动之前的元素
            ++curr;
    }

    return 0;
}