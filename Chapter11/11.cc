// 11 关联容器
/**
 * 关联容器和顺序容器有着根本的不同：关联容器中的元素是按关键字来保存和访问的。
 * 与之相对，顺序容器中的元素是按它们在容器中的位置来顺序保存和访问的。
 * 关联容器支持高效的关键字查找和访问。两个主要的关联容器（associative-container）类型是map和set。
 * 在某些文本处理过程中，可以用一个set来保存想要忽略的单词。字典则是一个很好的使用map的例子：可以将单词作为关键字，将单词释义作为值。
 * 标准库提供8个关联容器，如表11.1所示。
 * 1. map   关联数组：保存关键字-值对
 * 2. set   关键字即值，即只保存关键字的容器
 * 3. multimap  关键字可重复出现的map
 * 4. multiset  关键字可重复出现的set
 * 5. unorderd_map  用哈希函数组织的map
 * 6. unorderd_set  用哈希函数组织的set
 * 7. unorderd_multimap 哈希组织的map：关键字可以重复出现
 * 8. unorderd_multiset 哈希组织的set：关键字可以重复出现
 * 类型map和multimap定义在头文件map中；set和multiset定义在头文件set中；无序容器则定义在头文件unordered_map和unordered_set中。[插图]
 */

#include <iterator>
#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <string>
#include <array>
#include <stack>
#include <queue>
#include <algorithm>
#include <numeric>
using std::swap;
using std::vector, std::list, std::deque, std::forward_list, std::string, std::array, std::stack, std::queue;
#include "../Chapter07/Sales_data.h"
#include <iostream>
using std::begin, std::cbegin, std::end, std::cend, std::find, std::accumulate, std::equal, std::fill, std::fill_n, std::back_inserter;
using std::cin, std::cout, std::endl;
using std::copy, std::replace, std::replace_copy;

int main()
{
    

    return 0;
}