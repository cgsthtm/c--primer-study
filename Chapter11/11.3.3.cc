// 11.3 关联容器操作
/**
 * 除了表9.2（第295页）中列出的类型，关联容器还定义了表11.3中列出的类型。这些类型表示容器关键字和值的类型。[插图]
 * 1. key_type     此容器类型的关键字类型
 * 2. mapped_type  每个关键字关联的类型，即值的类型，只适用于map
 * 3. value_type   对于set，与key_type相同；对于map，为pair<const key_type,mapped_type>
 * 与顺序容器一样（参见9.2.2节，第297页），我们使用作用域运算符来提取一个类型的成员——例如，map<string，int>：：key_type。
 */

#include <iterator>
#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <string>
#include <array>
#include <stack>
#include <queue>
#include <algorithm>
#include <numeric>
using std::swap;
using std::vector, std::list, std::deque, std::forward_list, std::string, std::array, std::stack, std::queue;
#include "../Chapter07/Sales_data.h"
#include <iostream>
using std::begin, std::cbegin, std::end, std::cend, std::find, std::accumulate, std::equal, std::fill, std::fill_n, std::back_inserter;
using std::cin, std::cout, std::endl;
using std::copy, std::replace, std::replace_copy;
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <utility>
using namespace std;

int main()
{
    // 11.3.3 删除元素
    // 关联容器定义了三个版本的erase，如表11.5所示。
    // 1. c.erase(k);   从c中删除每个关键字为k的元素。返回一个size_type值，指出删除元素的数量
    // 2. c.erase(p);   从c中删除迭代器p指定的元素。p不能等于c.end()。返回一个指向p之后元素的迭代器，若p指向c的尾元素，则返回c.end()
    // 3. c.erase(b,e); 删除迭代器对b和e所表示的范围中的元素，返回e

    return 0;
}