/**
 * @brief 3.2.2 string对象上的操作
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
#include <iostream>
#include <string>
using std::cin, std::cout, std::endl;
using std::string, std::getline;
int main(int argc, char const *argv[])
{
    // 使用getline读取一整行
    string line;
    while(getline(cin,line)) // 使用getline，一次输出一整行
    {
        if(!line.empty()) // string的empty操作
            if(line.size() > 10) // string的size操作，size函数返回的是一个sting::size_type类型的值
                cout << line << endl;
    }

    // 比较string对象
    string str = "Hello";
    string phrase = "Hello World";
    string slang = "Hiya"; // slang > phrase > str

    // 两个string对象相加
    string s1 = "hello", s2 = "world\n";
    string s3 = s1 + s2;

    // 字面值和string对象相加
    string s4 = s1 + ","; // 正确，把一个string和一个字面值相加
    // string s5 = "hello" + ","; // 错误，两个运算对象都不是string
    // 因为某些历史原因，也为了与C兼容，所以C++语言中的字符串字面值并不是标准库类型string的对象。
    // 切记，字符串字面值与string是不同的类型。

    return 0;
}
