// 3.3.1 定义和初始化vector对象

#include <vector>
#include <string>
using std::string;
using std::vector;
int main(int argc, char const *argv[])
{
    // 列表初始化
    vector<string> v1{"a","b","c"}; // 正确
    //vector<string> v2("a","b","c"); // 错误
    vector<string> v3 = {"a","b","c"}; // 正确

    // 创建指定数量元素
    vector<int> vi(5,-1); // 5个int型元素，每个都被初始化为-1

    // 值初始化
    vector<int> ivec1(10); // 10个元素，每个都初始化为0
    vector<string> svec1(10); // 10个元素，每个都初始化为空string对象

    // 列表初始值还是元素数量
    vector<int> v4(4); // 4个元素，每个都初始化为0
    vector<int> v5{5}; // 1个元素，初始化为5
    vector<int> v6(6,1); // 6个元素，每个值都是1
    vector<int> v7{7,1}; // 2个元素，分别是7和1

    //vector<string> v8("hello"); // 错误，不能使用字符串字面值构建vector对象

    return 0;
}
