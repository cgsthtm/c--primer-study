/**
 * @brief 3.2.1定义和初始化string对象
 *
 * @return int
 */
#include <iostream>
#include <string>
using std::cin, std::cout, std::endl;
using std::string;
int main()
{
    string s1;          // 默认初始化，s1是个空字符串
    string s2 = s1;     // s2是s1的副本
    string s3 = "hiya"; // s3是该字符串字面值的副本
    string s4(10, 'c'); // s4的内容是cccccccccc，等价于string s4("cccccccccc");

    // 直接初始化和拷贝初始化
    // 如果使用等号（=）初始化一个变量，即拷贝初始化。反之，即直接初始化。

    string s;
    cin >> s;
    cout << s << endl;

    string word;
    while (cin >> word)
    {
        cout << word << endl;
    }

    return 0;
}