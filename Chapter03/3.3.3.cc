// 3.3.3 其他vector操作
// 除了push_back之外，vector还提供了几种其他操作，大多数都和string的相关操作类似

#include <iostream>
#include <vector>
#include <string>
using std::cin, std::cout, std::endl;
using std::string;
using std::vector;
int main(int argc, char const *argv[])
{
    // 范围for循环中不应改变其所遍历序列的大小
    vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9};
    for (auto &i : v)
    {
        i *= i; // 求元素的平方
    }
    for (auto i : v)
    {
        cout << i << " ";
    }
    cout << endl;

    // 要使用size_type，需首先指定它是由哪种类型定义的。
    vector<int>::size_type vi_cnt = 0; // 正确
    // vector::size_type vi_cnt = 0; // 错误

    // 计算vector内对象的索引
    // 假设有一组成绩的集合，其中成绩的取值是从0到100。以10分为一个分数段，要求统计各个分数段各有多少个成绩。
    vector<unsigned> scores(11, 0); // 11个分数段，都初始化为0
    unsigned grade;                 // 分数
    while (cin >> grade)
    {
        if (grade <= 100) // 有效输入
            ++scores[grade / 10]; // 用grade除以10来计算成绩所在的分数段
    }
    for (auto s : scores)
    {
        cout << s << " ";
    }
    cout << endl;

    // 不能用下标形式添加元素，而应该用vector的push_back成员函数
    vector<int> ivec;
    for(decltype(ivec.size()) ix=0; ix!=10; ++ix)
    {
        //ivec[ix] = ix; // 严重错误，ivec不包含任何元素
        ivec.push_back(ix); // 正确，添加一个新元素
    }

    return 0;
}
