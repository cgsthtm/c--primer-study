/**
 * @brief 3.1 命名空间的using声明
 * using namespace::name
 * @return int 
 */
#include <iostream>

// using声明
using std::cin;

int main()
{
    int i = 0;
    cin >> i; // 正确，cin和std::cin含义相同
    // cout << i; // 错误，没有对应的using声明，必须使用完整的名字
    std::cout << i; // 正确，显示地从std中使用cout
    return 0;
}