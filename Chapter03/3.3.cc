// 3.3 标准库类型vector
// 标准库类型vector表示对象的集合，其中所有对象的类型都相同。
// 集合中的每个对象都有一个与之对应的索引，索引用于访问对象。
// 因为vector“容纳着”其他对象，所以它也常被称作容器（container）。

/**
 * @brief C++语言既有类模板（class template），也有函数模板，其中vector是一个类模板。
 * 模板本身不是类或函数，相反可以将模板看作为编译器生成类或函数编写的一份说明。
 */

#include <vector>
#include <string>
using std::string;
using std::vector;
int main(int argc, char const *argv[])
{
    vector<int> ivec; // ivec保存int型对象
    vector<vector<string> > file; // 该向量的元素是vector对象

    // 3.3.1 定义和初始化vector对象

    // 列表初始化
    vector<string> v1{"a","b","c"}; // 正确
    //vector<string> v2("a","b","c"); // 错误
    vector<string> v3 = {"a","b","c"}; // 正确

    // 创建指定数量元素
    vector<int> vi(5,-1); // 5个int型元素，每个都被初始化为-1

    // 值初始化
    vector<int> ivec1(10); // 10个元素，每个都初始化为0
    vector<string> svec1(10); // 10个元素，每个都初始化为空string对象

    // 列表初始值还是元素数量
    vector<int> v4(4); // 4个元素，每个都初始化为0
    vector<int> v5{5}; // 1个元素，初始化为5
    vector<int> v6(6,1); // 6个元素，每个值都是1
    vector<int> v7{7,1}; // 2个元素，分别是7和1

    return 0;
}
