// 3.3.2 向vector对象中添加元素

#include <iostream>
#include <vector>
#include <string>
using std::cin, std::cout, std::endl;
using std::string;
using std::vector;
int main(int argc, char const *argv[])
{
    // vector的成员函数push_back
    vector<int> v2; // 空vector对象
    for(int i=0; i!=100; ++i)
    {
        v2.push_back(i); // 依次把整数值0~99放到v2尾端
    }

    // 从标准输入中读取单词
    string word;
    vector<string> svec;
    while(cin >> word)
        svec.push_back(word);

    // 注意！！范围for循环中不应改变其所遍历序列的大小

    return 0;
}
