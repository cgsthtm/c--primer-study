/**
 * @brief 3.5.2 访问数组元素
 * 在使用数组下标的时候，通常将其定义为size_t类型
 * size_t是一种机器相关的无符号类型，它被设计的足够大以便能表示内存中任意对象的大小。
 * 在cstddef头文件中定义了size_t类型，这个文件是C标准库stddef.h头文件的C++语言版本
 */
#include <iostream>
#include <string>
#include <cstddef>
using std::cin, std::cout, std::endl;
using std::string;
using std::size_t; // 在cstddef头文件中定义了size_t类型

int main(int argc, char const *argv[])
{
    // 数组除了大小固定这一特点外，其他用法与vector基本类似
    unsigned scores[11] = {}; // 11个分数段，初始化为0
    unsigned grade;
    while (cin >> grade)
    {
        if(grade <= 100)
            ++scores[grade/10]; // 将当前分数段的计数+1，假如grade是99分，那么99/10=9，说明该成绩是第9个分数段（下标从0开始）
    }
    for(auto i : scores)
    {
        cout << i << " "; // 输出当前计数
    }
    cout << endl;


    return 0;
}
