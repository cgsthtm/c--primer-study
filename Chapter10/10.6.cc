// 10.6 特定容器算法
/**
 * 与其他容器不同，链表类型list和forward_list定义了几个成员函数形式的算法，如表10.6所示。
 * 特别是，它们定义了独有的sort、merge、remove、reverse和unique。
 * list和forward_list成员函数版本的算法
 * 1. lst.merge(lst2)      将来自lst2的元素合并入lst。lst和lst2必须是有序的。
 * 2. lst.merge(lst2,comp) 元素将从lst2中删除。合并之后，lst2变为空。comp是给定的比较操作。
 * 3. lst.remove(val)      调用erase删除掉与给定值相等的元素。
 * 4. lst.remove_if(pred)  调用erase删除掉令一元谓词为真的每个元素。
 * 5. lst.reverse()        反转lst中元素的顺序。
 * 6. lst.sort()           使用<排序元素。
 * 7. lst.sort(comp)       使用给定的comp比较操作来排序元素。
 * 8. lst.unique()         调用erase删除同一个值的连续拷贝，使用==。
 * 9. lst.unique(pred)     调用erase删除同一个值的连续拷贝，使用给定的二元谓词。
 * 一个链表可以通过改变元素间的链接而不是真的交换它们的值来快速“交换”元素。因此，这些链表版本的算法的性能比对应的通用版本好得多。
 * 对于list和forward_list，应该优先使用成员函数版本的算法而不是通用算法。
 */

#include <iterator>
#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <string>
#include <array>
#include <stack>
#include <queue>
#include <algorithm>
#include <numeric>
#include <functional>
#include <iostream>
using std::swap;
using std::vector, std::list, std::deque, std::forward_list, std::string, std::array, std::stack, std::queue;
#include "../Chapter07/Sales_data.h"
#include "../Chapter01/Sales_item.h"
using std::begin, std::cbegin, std::end, std::cend, std::find, std::accumulate, std::equal, std::fill, std::fill_n, std::back_inserter;
using std::cin, std::cout, std::endl;
using std::copy, std::replace, std::replace_copy, std::sort, std::unique, std::stable_sort, std::find_if, std::for_each;
using std::ostream, std::ref;
using std::transform, std::bind, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3;
using namespace std;

int main()
{
    // splice成员
    // 链表类型还定义了splice算法，其描述见表10.7。此算法是链表数据结构所特有的，因此不需要通用版本。[插图]
    // list和forward_list的splice成员函数的参数
    // lst.splice(args)或flst.splice_after(args)
    // 1. (p, lst2)       p是个指向lst中元素的迭代器，或是个指向flst首前位置的迭代器。
    //                    函数将lst2的所有元素移动到lst中p之前的位置或是flst中p之后的位置。将元素从lst2中删除。lst与lst2不能是同一个链表。
    // 2. (p, lst2, p2)   p2是个指向lst2中位置的有效的迭代器。将p2指向的元素移动到lst中，或将p2之后的元素移动到flst中。lst与lst2可以是相同链表。
    // 3. (p, lst2, b, e) b与e必须表示lst2中的合法范围。lst与lst2可以是相同链表。

    // 链表特有的操作会改变容器
    // 链表特有版本与通用版本间的一个至关重要的区别是链表版本会改变底层的容器。
    // 例如，remove的链表版本会删除指定的元素。unique的链表版本会删除第二个和后继的重复元素。
    // 链表版本的merge函数会销毁给定的链表——元素从参数指定的链表中删除，被合并到调用merge的链表对象中。

    return 0;
}
