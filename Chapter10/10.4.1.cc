// 10.4 再探迭代器
/**
 * 除了为每个容器定义的迭代器之外，标准库在头文件iterator中还定义了额外几种迭代器。
 * 1. 插入迭代器：被绑定到一个容器上，用来向容器插入元素。
 * 2. 流迭代器：被绑定到输入或输出流上，用来遍历所关联的IO流。
 * 3. 反向迭代器：向后而不是向前移动，除了forward_list之外的标准库容器都有反向迭代器。
 * 4. 移动迭代器：这些专用的迭代器不拷贝其中的元素，而是移动它们。
 */

#include <iterator>
#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include <string>
#include <array>
#include <stack>
#include <queue>
#include <algorithm>
#include <numeric>
#include <functional>
#include <ostream>
using std::swap;
using std::vector, std::list, std::deque, std::forward_list, std::string, std::array, std::stack, std::queue;
//#include "../Chapter07/Sales_data.h"
#include <iostream>
using std::begin, std::cbegin, std::end, std::cend, std::find, std::accumulate, std::equal, std::fill, std::fill_n, std::back_inserter;
using std::cin, std::cout, std::endl;
using std::copy, std::replace, std::replace_copy, std::sort, std::unique, std::stable_sort, std::find_if, std::for_each;
using std::transform, std::bind, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3;
using std::ostream,std::ref;
using namespace std;

int main()
{
    // 10.4.1 插入迭代器
    // 当我们通过一个插入迭代器进行赋值时，该迭代器调用容器操作来向给定容器的指定位置插入一个元素。
    // 插入器有三种类型，差异在于元素插入的位置：
    // 1. back_inserter创建一个使用push_back的迭代器
    // 2. front_inserter创建一个使用push_front的迭代器
    // 3. inserter创建一个使用insert的迭代器
    list<int> lst = {1,2,3,4};
    list<int> lst2,lst3; // 空list
    copy(lst.cbegin(), lst.cend(), front_inserter(lst2)); // 拷贝完成后，lst2包含4 3 2 1
    copy(lst.cbegin(), lst.cend(), inserter(lst3,lst3.begin())); // 拷贝完成后，lst3包含1 2 3 4

    return 0;
}
