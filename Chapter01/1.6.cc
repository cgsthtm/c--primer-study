#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item total; // 保存下一条交易记录的变量
    if (std::cin >> total)
    {
        Sales_item trans; // 保存和的变量
        while (std::cin >> trans)
        {
            if (total.isbn() == trans.isbn()) // 如果ISBN相同，则相加
                total += trans;
            else
            {
                std::cout << total << std::endl;
                total = trans;
            }
        }
        std::cout << total << std::endl; // 输出最后一条记录
    }
    else
    {
         std::cout << "No data?!" << std::endl;
         return -1; // 表示失败
    }

    return 0;
}