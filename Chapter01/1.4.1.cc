#include <iostream>
int main()
{
    int sum = 0, val = 1;
    while (val <= 10)
    {
        std::cout << "first val:" << val << std::endl;
        sum += val;
        ++val;
        std::cout << "last val:" << val << std::endl;
    }
    std::cout << "Sum of 1 to 10 inclusive is " << sum << std::endl;
    return 0;
    /* 输出结果
    first val:1
    last val:2
    first val:2
    last val:3
    first val:3
    last val:4
    first val:4
    last val:5
    first val:5
    last val:6
    first val:6
    last val:7
    first val:7
    last val:8
    first val:8
    last val:9
    first val:9
    last val:10
    first val:10
    last val:11
    Sum of 1 to 10 inclusive is 55
    */
}